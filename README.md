Compare Nunit-Xunit Syntax: https://xunit.net/docs/comparisons

Testing controller: https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/testing?view=aspnetcore-3.1

Testing reporstory: https://www.carlrippon.com/testing-ef-core-repositories-with-xunit-and-an-in-memory-db/

Inline Data: https://andrewlock.net/creating-parameterised-tests-in-xunit-with-inlinedata-classdata-and-memberdata/