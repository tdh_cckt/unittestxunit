﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Repositories;

namespace WebApi.Services
{
    public class StudentService : IStudentService
    {
        private readonly IStudentReporsitory _studentReporsitory;

        public StudentService(IStudentReporsitory studentReporsitory)
        {
            _studentReporsitory = studentReporsitory;
        }
        public bool Add(Student student)
        {
            if (student.Code == null)
                throw new Exception("Empty student code");

            var existingStudent = _studentReporsitory.GetByCode(student.Code);
            if (existingStudent != null)
            {
                return false;
            }

            _studentReporsitory.Add(student);

            return true;     
        }
    }
}
