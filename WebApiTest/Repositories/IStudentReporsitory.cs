﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Repositories
{
    public interface IStudentReporsitory
    {
        ICollection<Student> GetAll();
        Student GetByCode(string code);
        void Add(Student student);
    }
}
