﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Repositories
{
    public class StudentRepository : IStudentReporsitory
    {
        private readonly StudentDbContext _studentDbContext;

        public StudentRepository(StudentDbContext studentDbContext)
        {
            _studentDbContext = studentDbContext;
        }
        public void Add(Student student)
        {
            _studentDbContext.Students.Add(student);
        }

        public ICollection<Student> GetAll()
        {
            return _studentDbContext.Students.ToList();
        }

        public Student GetByCode(string code)
        {
            return _studentDbContext.Students.FirstOrDefault(c => c.Code == code);
        }
    }
}
