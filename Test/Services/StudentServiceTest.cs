﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using WebApi.Models;
using WebApi.Repositories;
using WebApi.Services;
using Xunit;

namespace WebApiTestTest.Services
{
    public class StudentServiceTest
    {
        private readonly StudentService _studentService;
        private readonly Mock<IStudentReporsitory> _studentReporsitory;
        public StudentServiceTest()
        {
            _studentReporsitory = new Mock<IStudentReporsitory>();
            _studentService = new StudentService(_studentReporsitory.Object);
        }

        [Fact]
        public void Add_StudenCodeNull_ThrowException()
        {
            var student = new Student()
            {
                Code = null
            };
            Assert.Throws<Exception>(() => _studentService.Add(student));           
        }

        [Fact]
        public void Add_StudentInValidExistingStudentCode_ReturnFalse()
        {
            var student = new Student()
            {
                Code = "abc"
            };

            var studentForReturn = new Student()
            {
                Name = "Name",
                Code = "abc"
            };

            _studentReporsitory.Setup(c => c.GetByCode(It.IsAny<string>())).Returns(studentForReturn);

            var result = _studentService.Add(student);

            Assert.False(result);

            _studentReporsitory.Verify(c => c.GetByCode(It.IsAny<string>()), Times.Once);
        }
    }
}
