﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApi;
using WebApi.Models;
using WebApi.Repositories;
using Xunit;

namespace WebApiTestTest.Repositories
{
    public class StudentRepositoryTest
    {
        [Fact]
        public void Add()
        {
            StudentDbContext context = GetInMemoryPersonRepository();

            Student student = new Student()
            {
                Name = "Name",
                Code = "Code",
                Id = 5
            };

            context.Students.Add(student);
           var find = context.Students.Find(5);

            Assert.Equal("Name", find.Name);
        }

        private StudentDbContext GetInMemoryPersonRepository()
        {
            DbContextOptions<StudentDbContext> options;
            var builder = new DbContextOptionsBuilder<StudentDbContext>();
            builder.UseInMemoryDatabase("Student");
            options = builder.Options;
            StudentDbContext studenDataContext = new StudentDbContext(options);
            studenDataContext.Database.EnsureDeleted();
            studenDataContext.Database.EnsureCreated();
            return studenDataContext;
        }
    } 
}
