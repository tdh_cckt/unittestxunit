﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using WebApi.Controllers;
using WebApi.Models;
using WebApi.Repositories;
using WebApi.Services;
using Xunit;

namespace WebApiTestTest.Controllers
{
    public class StudentControllerTest
    {
        private readonly StudentController _controller;
        private readonly Mock<IStudentService> _studentService;
        public StudentControllerTest()
        {
            _studentService = new Mock<IStudentService>();
            _controller = new StudentController(_studentService.Object);
        }

        [Fact]
        public void Post_InValidModelState_ReturnBadRequest()
        {
            _controller.ModelState.AddModelError("Code", "Required");

            var student = new Student();

            var result = _controller.Post(student);
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.IsType<SerializableError>(badRequestResult.Value);
        }

        [Fact]
        public void Post_InValidAddStudent_ReturnBadRequest()
        {
            var student = new Student();

            _studentService.Setup(c => c.Add(It.IsAny<Student>())).Returns(false);

            var result = _controller.Post(student);
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Existing Code", badRequestResult.Value);

            _studentService.Verify(c => c.Add(It.IsAny<Student>()), Times.Once);
        }

        [Fact]
        public void Post_Success_ReturnStudent()
        {
            var student = new Student()
            { 
                Code = "Code",
                Name = "Name"
            };

            _studentService.Setup(c => c.Add(It.IsAny<Student>())).Returns(true);

            var result = _controller.Post(student);
            var returnValue = Assert.IsType<OkObjectResult>(result);          
            Assert.IsType<Student>(returnValue.Value);

            _studentService.Verify(c => c.Add(It.IsAny<Student>()), Times.Once);
        }
    }
}
